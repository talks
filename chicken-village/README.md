## About

A talk about doing low-level graphics programming in elisp using XCB
and the XELB library. The slides themselves are programmed in elisp.
