Waka ist eine MIDI-Shell mit der man Musik komponieren kann, für die
Tastatur optimiert und ohne den Ballast einer herkömmlichen Digital
Audio Workstation.  Das Geheimnis dahinter ist eine Kombination aus
MIDI, dem universellen Standard für die Übertragung und Kodierung von
Noten sowie der Programmiersprache Scheme, einem eleganten und
vielfach implementierten Lisp-Dialekt.

Der Vortrag geht auf die Motivation, Fähigkeiten und die
Implementierung von Waka ein, zusätzlich dazu werden die Fähigkeiten
der Software demonstriert.

[OpenChaos-Ankündigung], [Videoaufnahme].

[OpenChaos-Ankündigung]: https://koeln.ccc.de/updates/2017-10-26_OC-Waka.xml
[Videoaufnahme]: https://media.ccc.de/v/c4.openchaos.2017.10.klavier-spielen-mit-kawa-scheme
