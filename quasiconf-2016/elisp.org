* Language

** Data types

- Integers
- Floats
- Symbols
- Conses
- Strings
- Vectors, char-table, bool-vector
- Hash tables
- Macro (macro . (lambda ...))
- Function (lambda ...), subr, byte-code function
- Autoload (autoload ...)

** Editing types

...

** Type hierarchy

- Sequence (ordered)
  - Lists (extensible, made of Cons cells)
  - Arrays (fixed-size)
    - String (chars)
    - Vector (general-purpose)
    - Char-table (chars as indices)
    - Bool-vector (ones and zeroes, bitstring in other languages)

** Equality predicates

* APIs
