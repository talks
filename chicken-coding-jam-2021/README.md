## About

At SecCamp 2019 I've attended a talk about how one builds their own
[Rubberducky] replacement using a cheap [Digistump Digispark]
microcontroller. The device is programmed using [Duckyscript], a
BASIC-style language describing human keyboard input. Using that
device one can now execute commands on other people's computers at
high speed, a useful capability during read teaming security
assessments.

During the workshop I've discovered that the toolchain to arm both
official Rubberducky hardware and cheap microcontrollers leaves a lot
to be desired, so I started hacking on a more powerful replacement
faithfully reimplementing the original behavior. This talk explains
the basics behind HID devices enabling this kind of hardware and the
challenges of writing [plucky], a Duckyscript compiler.

[Rubberducky]: https://github.com/hak5darren/USB-Rubber-Ducky
[Digistump Digispark]: http://digistump.com/category/1
[Duckyscript]: https://github.com/hak5darren/USB-Rubber-Ducky/wiki/Duckyscript
[plucky]: https://depp.brause.cc/plucky/
