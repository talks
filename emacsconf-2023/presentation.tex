% Created 2023-11-20 Mon 23:01
% Intended LaTeX compiler: pdflatex
\documentclass[presentation]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{tabu}
\usepackage{minted}
\usepackage[english, american]{babel}
\hypersetup{pdfauthor="Vasilij Schneidermann", pdftitle="Speedcubing in Emacs", colorlinks, linkcolor=, urlcolor=blue}
\setminted{fontsize=\footnotesize,escapeinside=||}
\usetheme{Rochester}
\usecolortheme[RGB={87,83,170}]{structure}
\author{Vasilij Schneidermann}
\date{November 2023}
\title{Speedcubing in Emacs}
\hypersetup{
 pdfauthor={Vasilij Schneidermann},
 pdftitle={Speedcubing in Emacs},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.1 (Org mode 9.6.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\begin{frame}{Outline}
\tableofcontents
\end{frame}

\AtBeginSection{\frame{\sectionpage}}

\section{Intro}
\label{sec:orgd2a3b44}

\begin{frame}[label={sec:orge8e0dbe}]{About}
\begin{itemize}
\item Vasilij Schneidermann, 31
\item Cyber security consultant at CODE WHITE GmbH
\item mail@vasilij.de
\item \url{https://depp.brause.cc/}
\item \url{https://emacsninja.com/}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgc530663}]{Motivation and challenges involved}
\begin{itemize}
\item I picked up speedcubing again
\item Cubing hardware has improved a lot
\item Scrambling and timing essential for getting better
\item Seemingly all good options are web/mobile (cstimer, twisty-timer)
\item No decent options inside Emacs
\item Case study how to do better
\item Making use of new Emacs features (svg.el, transient, sqlite)
\item Most importantly: Having fun
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgf07c754}]{Prior art}
\begin{itemize}
\item \url{https://github.com/rafoo/stimer} (timer, shows stats)
\item \url{https://github.com/param108/rubiks.el} (solver)
\item \url{https://github.com/deadblackclover/scramble-el} (text-only scrambler)
\item \url{https://github.com/queyenth/rubik.el} (scrambler, unfinished timer)
\item \url{https://github.com/Kurvivor19/rubik-mode} (cube simulator)
\item \url{https://gitlab.com/funkenblatt/elisp/-/blob/master/rubik.el} (group theory)
\item \url{https://github.com/lukasjuhrich/cubing} (algorithm notation)
\item \url{https://codeberg.org/akib/emacs-cube} (3D cube simulator)
\item None of those does it all :(
\end{itemize}
\end{frame}

\section{The wca-prep package}
\label{sec:org513d677}

\begin{frame}[label={sec:orgc5682cd}]{WCA what?}
\begin{itemize}
\item Coming up with a good name was difficult
\item World Cube Association holds events for twisty puzzles
\item They standardized the puzzles, rules and notation
\item Goal of my package: Helping people prepare for a WCA competition
\item Therefore the name wca-prep was chosen
\item This requires: Scrambler, scramble visualization, timer, stats
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgfc79670}]{Non-goals}
\begin{itemize}
\item To get somewhere, scope needs to be kept small
\item Focus on 3x3x3 cube for now, expand later
\item Out of scope:
\begin{itemize}
\item Solving the cube (the speedcuber does that)
\item Simulating a cube (simulated cubes are awkward to use)
\item Non-WCA puzzles (there are a ton, many 3x3x3 variations, some are
challenging to visualize)
\item Exotic WCA events (fewest moves challenge, blind solves)
\item Specialized scrambles (helping with algorithm practice)
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org71cf5cb}]{Demo time}
\begin{figure}[htbp]
\centering
\includegraphics[height=5cm]{./img/screenshot.png}
\caption{User interface}
\end{figure}
\end{frame}

\begin{frame}[label={sec:org5c441c3}]{Challenge: Puzzle representation}
\begin{itemize}
\item Many possible representations for cube state
\item There is no obvious best representation
\item Since programmatic solving is out of scope, I picked the easiest
possible one (array of numbered facelets)
\item Awkward to do scrambles on, but otherwise good enough
\item Future: Learn group theory, pick better representation
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org0135928},fragile]{Challenge: Scrambling}
 \begin{itemize}
\item After deciding on a representation, this should be easy
\item A turn ends up swapping several facelets with other facelets
\item Determining which facelets was very confusing
\item One helpful suggestion: Make a paper cube with numbers on it
\item \texttt{cl-rotatef} was highly useful for in-place swapping
\item Future: Make use of group theory and better representation
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgb8f4b9a}]{Challenge: Scrambling}
\begin{figure}[htbp]
\centering
\includegraphics[height=6cm]{./img/papercube.jpg}
\caption{Two very different cubes}
\end{figure}
\end{frame}

\begin{frame}[label={sec:org2d2bbbb}]{Challenge: Scramble algorithm}
\begin{itemize}
\item WCA: 3x3x3 cube is scrambled by generating a random cube, solving it
and emitting the sequence of solve moves
\item WCA: Solve moves are canonicalized (folding subsequent moves when
possible)
\item Example: Rotate 90° CW, rotate 90° CW -> rotate 180° CW
\item Seemingly all other scramblers: Generate random moves, some
canonicalize
\item This scrambler: Generates random move, canonicalizes with previous
move, repeats until desired amount of moves has been generated
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org7bd111f}]{Challenge: Scramble visualization}
\begin{itemize}
\item Mapping between every facelet and unfolded puzzle view was generated
\item Out of scope: Facelet orientation (non-WCA puzzles would require
this)
\item Initial prototype used colored text
\item svg.el turned out to be easy enough to use
\item Future: Eliminate hardcoded puzzle face/color mapping for theming
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgb9b132e}]{Challenge: Intuitive UI with transient}
\begin{itemize}
\item Idea: Self-documenting, magit-style UI
\item Transient has become part of Emacs, so why not
\item Issue: Documentation is difficult to understand, unclear what best
practices are or how things should be done
\item transient-showcase helps with examples, they feel half-done though
\item Issue: Unhelpful error messages when using transient wrong
\item Future: Find more packages using it and try to make better use of it
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgd5ad52a}]{Challenge: SQLite book-keeping}
\begin{itemize}
\item SQLite support landed in Emacs recently (29.1)
\item It's still early days (found a bug in transaction macro)
\item API is very basic, convenience helpers are missing
\item The biggest challenge was figuring out a decent schema
\item Finally no more persistence issues
\end{itemize}
\end{frame}

\section{Outro}
\label{sec:org6a9a46c}

\begin{frame}[label={sec:org9e7bd55}]{Lessons learned}
\begin{itemize}
\item There's still plenty packages for Emacs to be written
\item The 3x3x3 cube does not require advanced mathematics
\item However: It proves there's finally a real-world use for group theory
\item The new Emacs features are good enough, but need more testing
\item Call for action: Please make use of SQLite, report bugs, \ldots{}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org7b31972}]{What next?}
\begin{itemize}
\item More testing and UI improvements
\item Better stats
\item More puzzles
\item \ldots{}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgccf439d}]{Questions?}
\end{frame}
\end{document}