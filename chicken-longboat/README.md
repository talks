## About

Contains slides and code for a talk demonstrating that interesting
attacks on crypto (as borrowed from [the Cryptopals exercises]) don't
have to be hard.

[the Cryptopals exercises]: https://www.cryptopals.com
