# Krypto knacken für Anfänger

Moderne Kryptographie ist ein wichtiges Feld der angewandten
Mathematik.  Die dadurch erarbeiteten Bausteine, sogenannte
kryptographische Primitive, werden als sicher bewiesen, jedoch gilt
diese Eigenschaft nur wenn diese korrekt verwendet werden.  Bei dem
Kombinieren dieser Bausteine zu einem kryptographischen System kommt
es oft zu Fehlern welche die gesamte Sicherheit des Systems
kompromittieren können.

---

In diesem Vortrag werden bekannte Angriffe auf solche Systeme
präsentiert, dafür werden Übungen von der Website
https://cryptopals.com/ verwendet und mit überraschend wenigen Zeilen
Code in der Programmiersprache Ruby gelöst.
