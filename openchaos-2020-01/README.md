Gopher ist ein weitestgehend in Vergessenheit geratenes Protokoll für
den Abruf von Dokumenten.  Es ähnelt am ehesten dem Web 1.0, jedoch
verwendet es eine strikt hierarchische Struktur und verzichtet bewusst
auf komplexe Features wie Markup, Styling und Ausführung von
Skriptsprachen.  Trotz des Erfolges des World Wide Web existiert
weiterhin eine Gruppe von Gopher-Enthusiasten, welche sogenannte
Gophersites betreiben.

In diesem Vortrag wird eine kurze Einführung in das Gopher-Protokoll
gegeben und anschließend gezeigt wie das komplette IPv4-Netz nach
Gopher-Services durchsucht und die gewonnenen Daten einer Analyse
unterzogen wurden.  Mithilfe dieser Daten wird versucht zu beantworten
wer genau auf Port 70 lauscht und welche Inhalte heutzutage angeboten
werden.

Video auf [media.ccc.de].

[media.ccc.de]: https://media.ccc.de/v/c4.openchaos.2020.01.gopherspace-mapping
