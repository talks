Talks I've held at various conferences, ordered in chronological order.

- [Quasiconf 2014](quasiconf-2014): Emacs Lisp or Why Emacs' Extension Language Is Worth Another Look
- [Quasiconf 2015](quasiconf-2015): Emacs as my Canvas
- [Quasiconf 2016](quasiconf-2016): MAL - Making a Lisp
- [CHICKEN Sun and Fun Conference 2016](chicken-summer): Explorations in GUI programming with CHICKEN
- [Openchaos 2017-01](openchaos-2017-01): MAL - Making a Lisp
- [Openchaos 2017-06](openchaos-2017-06): Interim - Zwischen Plan 9 und Lisp Machine
- [CHICKEN in the forest](chicken-in-the-forest): The state of R7RS - Implementing MAL in portable Scheme, Playing the piano with Kawa
- [Openchaos 2017-10](openchaos-2017-10): Klavier spielen mit Kawa Scheme
- [Longboat CHICKEN](chicken-longboat): Knowing Just Enough Crypto to be Dangerous
- [Openchaos 2018-10](openchaos-2018-10): Krypto knacken für Anfänger
- [SaarCHICKEN 2019](chicken-saar): State of Retro Gaming in Emacs
- [Openchaos 2019-11](openchaos-2019-11): State of Retro Gaming in Emacs
- [SecCamp 2019](seccamp-2019): Knowing just enough crypto to be dangerous, Actually using radare2, WTF CTF
- [Openchaos 2020-01](openchaos-2020-01): Auf der Suche nach dem verlorenen Internet
- [Emacsconf 2020](emacsconf-2020): State of Retro Gaming in Emacs
- [CHICKEN Online Coding Jam 2021](chicken-coding-jam-2021): Towards a better Duckyscript toolchain
- [Village CHICKENs 2023](chicken-village): X11 programming in Emacs Lisp - A descent into madness
- [Emacsconf 2023](emacsconf-2023): Speedcubing in Emacs
