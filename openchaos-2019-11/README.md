Emacs ist für manche Menschen ein erweiterbarer Texteditor, für andere
ein großartiges Betriebssystem.  Weitaus weniger bekannt ist, dass es
möglich ist graphische Demos und Spiele für Emacs zu programmieren,
unter anderem sogar Emulatoren für bekannte Spiele-Plattformen wie der
NES-Konsole und dem GameBoy.

In diesem Vortrag werden die Fähigkeiten von Emacs jenseits dem
Editieren von Text betrachtet, einige Emulatoren demonstriert und
schließlich das chip8.el-Projekt des Vortragenden im Detail
vorgestellt.  Fokus liegt dabei auf den Eigenheiten der
Spieleplattform CHIP-8, der Funktionsweise des Emulators sowie den
Herausforderungen, welche bei der Umsetzung aufgetreten sind.
