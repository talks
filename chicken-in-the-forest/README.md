## About

Contains slides for a talk on my explorations in R7RS and my most
involved Scheme project so far, [waka].

[waka]: https://github.com/wasamasa/waka
