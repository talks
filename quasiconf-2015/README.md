## About

This talk explains the basics and techniques behind graphical demos in
Emacs.  See `presentation.org` and `presentation.pdf` for the slides.

Video recording: <https://youtu.be/NBArWrn6FnY>

I did a lightning talk later for [EmacsConf 2015][]:
<https://youtu.be/6LFtbwjWeEE>.  This prompted a hangout with [Sacha
Chua][]: <https://youtu.be/x1t9b7Fqo9c>.

## Setup

None this time.

[EmacsConf 2015]: http://emacsconf2015.org/
[Sacha Chua]: http://sachachua.com/blog/
