MAL ist ein minimaler, Clojure-inspirierter Lisp-Dialekt.  Ziel des
Projektes ist es, dem geneigten Programmierer zu vermitteln wie man
einen eigenen Interpreter konstruiert.  In diesem Vortrag werden
sowohl die Sprache als auch der Weg durch die einzelnen
Implementierungsschritte präsentiert.

Basiert auf Material von [einem älteren Vortrag].  Mehr unter
[koeln.ccc.de].  Video auf [media.ccc.de].

  [einem älteren Vortrag]: /quasiconf-2016
  [koeln.ccc.de]: https://koeln.ccc.de/updates/2017-01-20_OC-MAL.xml
  [media.ccc.de]: https://media.ccc.de/v/c4.openchaos.2017.01.make-a-lisp
