MAL ist ein minimaler, Clojure-inspirierter Lisp-Dialekt. Ziel des
Projektes ist es, dem geneigten Programmierer zu vermitteln wie man
einen eigenen Interpreter konstruiert. In diesem Vortrag werden sowohl
die Sprache als auch der Weg durch die einzelnen
Implementierungsschritte präsentiert.

---

Lisp ist eine bekannte Familie von Programmiersprachen welche sich
durch ein hohes Maß an Flexibilität sowie minimalistische Syntax
auszeichnen. In den Bereichen Lehre und Forschung hat sich der
Lisp-Dialekt Scheme als besonders geeignet herausgestellt für welchen
zahlreiche Implementierungen existieren; jedoch ist die Umsetzung
eines eigenen Scheme-Interpreters bei weitem nicht trivial.

Das MAL-Projekt versucht diese Lücke zu füllen indem es eine
vereinfachte, aber dennoch nützliche Sprache beschreibt und den
Programmierer mithilfe eines Guides sowie einem TDD-inspirierten
Testgerüst durch die einzelnen Stufen des Interpreters führt. Weitere
Informationen zu MAL können im
[kanaka/mal Projekt auf GitHub](https://github.com/kanaka/mal)
gefunden werden.
